package user;

import base_test.BaseTest;
import org.testng.annotations.Test;

public class CreateUserAndGetUser extends BaseTest {

    @Test
    public void createUserAndGetUser() {
        String userId = createRandomUser();

        getUser(userId);
    }
}
