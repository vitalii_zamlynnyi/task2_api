package user;

import base_test.BaseTest;
import org.testng.annotations.Test;

public class CreateUserAndDeleteUser extends BaseTest {

    @Test
    public void createUserAndDeleteUser() {
        String userId = createRandomUser();

        deleteUser(userId);
    }
}
