package user;

import base_test.BaseTest;
import models.User;
import org.testng.annotations.Test;

public class CreateUserAndUpdateWithPut extends BaseTest {

    @Test
    public void createUserAndUpdateWithPut() {
        String userId = createRandomUser();

        updateUser(userId, User.getRandomUser());
    }

}
