package base_test;

import com.google.gson.JsonObject;
import io.qameta.allure.Step;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import models.User;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;

public abstract class BaseTest {

    public static ThreadLocal<JsonObject> USER = new ThreadLocal<>();

    public static JsonObject getUser() {
        return USER.get();
    }

    @BeforeClass
    public void setUp() {
        RestAssured.baseURI = "https://reqres.in";
        RestAssured.basePath = "/api/users";
        USER.set(User.getRandomUser());
    }

    @Step
    public String createRandomUser() {
        String userId =
                RestAssured.given()
                        .filter(new AllureRestAssured())
                        .body(USER.get().toString())
                        .when()
                        .log().everything()
                        .post()
                        .then()
                        .log().body()
                        .assertThat()
                        .statusCode(201)
                        .extract().body().jsonPath().getString("id");
        Assert.assertNotNull(userId, "User id is Null");

        return userId;
    }

    @Step
    public void getUser(String userId) {
        RestAssured.given()
                .filter(new AllureRestAssured())
                .pathParam("userId", userId)
                .when()
                .get("/{userId}")
                .then()
                .log().body()
                .assertThat()
                .statusCode(200);
    }

    @Step
    public void updateUser(String userID, JsonObject newUserData) {
        RestAssured.given()
                .filter(new AllureRestAssured())
                .pathParam("userId", userID)
                .body(newUserData.toString())
                .when()
                .put("/{userId}")
                .then()
                .log().body()
                .assertThat()
                .statusCode(200);
    }

    @Step
    public void deleteUser(String userId) {
        RestAssured.given()
                .filter(new AllureRestAssured())
                .pathParam("userId", userId)
                .when()
                .delete("/{userId}")
                .then()
                .log().body()
                .assertThat()
                .statusCode(204);
    }
}
