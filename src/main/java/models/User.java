package models;

import com.google.gson.JsonObject;
import org.apache.commons.lang3.RandomStringUtils;

public class User {
    public static JsonObject getRandomUser() {
        JsonObject user = new JsonObject();
        user.addProperty("name", RandomStringUtils.randomAlphabetic(5));
        user.addProperty("job", RandomStringUtils.randomAlphabetic(5));
        return user;
    }
}
