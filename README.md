
## Running the tests

To run the tests you need to have Maven installed and maven system variable configured.
To generate a report you need to have Allure installed and allure system variable configured.

1. To run parallel api tests execute the command in the command line:
   ```mvn test```
2. To generate and open the allure report run the following in the CMD:
   ```start AllureServe.bat```

